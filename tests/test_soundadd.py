import unittest
from soundops import Sound, SoundSin, soundadd


class TestSoundAdd(unittest.TestCase):

    def test_same_length(self):
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(1, 880, 150)
        result = soundadd(s1, s2)
        self.assertNotEqual(result.duration, 44100)

    def different_length(self):
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(2, 880, 150)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.duration), 44100 * 2)

    def test_different_duration(self):
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(1, 880, 150)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 1)

if __name__ == '__main__':
    unittest.main()
