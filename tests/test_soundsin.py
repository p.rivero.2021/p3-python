import unittest
from mysound import Sound
from mysoundsin import SoundSin

class test_sound_sin(unittest.TestCase):

    def test_init(self):
        sound = SoundSin(5, 440, 10000)
        self.assertEqual(sound.frequency, 440)  # Comprobar frecuencia
        self.assertEqual(sound.amplitude, 10000)  # Comprobar amplitud
        self.assertEqual(sound.nsamples, 44100 * 5)  # Comprobar numero de muestras
        self.assertEqual(len(sound.buffer), 44100 * 5)  # Comprobar buffer

    def test_get_duration(self):
        sound = SoundSin(2, 440, 10000)
        self.assertEqual(sound.duration, 2)  # Comprobar duracion

    def test_sin_bars(self):
        sound_Sin = SoundSin(10,440,8000)

        bars_result = sound_Sin.bars()
        self.assertNotEqual(bars_result, "")


if __name__ == '__main__':
    unittest.main()
