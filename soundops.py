from mysound import Sound


class SoundSin(Sound):

    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)  # Inicializar clase padre
        self.sin(frequency, amplitude)


def soundadd(s1: Sound, s2: Sound) -> Sound:

    # Crear sound resultante con duración máxima
    result_duration = max(s1.duration, s2.duration)

    # Inicializar sound resultante
    result_sound = Sound(result_duration)

    # Recorrer muestras de s1 y s2 sumándolas
    for i in range(result_sound.nsamples):
        if i < s1.nsamples and i > s2.nsamples:   # Copiar muestras restantes
            # del sound más largo
            result_sound.buffer[i] = s1.buffer[i] + s2.buffer[i]
        elif i < s1.nsamples:  # Copiar muestras de s1
            result_sound.buffer[i] = s1.buffer[i]
        elif i > s2.nsamples:  # Copiar muestras de s2
            result_sound.buffer[i] = s2.buffer[i]

        return result_sound
